import pkg from './package.json'
import { defineNuxtConfig } from '@nuxt/bridge'

export default defineNuxtConfig({
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  ssr: true,
  loading: {
    color: '#00c5ff',
    height: '5px'
  },
  publicRuntimeConfig: {
    version : pkg.version,
  },
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Raynold\'s Headless Blog',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: 'Welcome to Raynold Chow\'s blog where I post about technology related topics. ' +
          'Sometimes I will also write articles involving bits of my daily life. This blog is started due to a ' +
          'trial implementation of headless structure using WordPress, but continues to be updated as a hobby since then.',
      }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'apple-touch-icon', href: '/apple-touch-icon.png' },
      { rel: 'icon' , sizes: '192x192', href: '/android-chrome-192x192.png'}
    ],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '@/assets/css/main'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
    '@nuxtjs/dotenv',
    '@nuxtjs/google-fonts',
    ['@nuxtjs/fontawesome', {
      icons: {
        brands: [
          'faLinkedinIn',
          'faTwitter',
          'faGitlab',
        ]
      }
    }],
  ],

  googleFonts: {
    download: true,
    base64: false,
    families: {
      Inter: [400, 600, 700]
    },
    display: 'swap',
  },

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/auth-next',
    'nuxt-change-case',
    [
      'nuxt-cookie-control', {
      barPosition: 'bottom-full',
    }
    ]
  ],
  //Cookies settings for nuxt cookie control
  cookies: {
    necessary: [
      {
        name: {
          en: 'Default Cookies'
        },
        description: {
          en: 'Used for cookie control.'
        },
        cookies: ['cookie_control_consent', 'cookie_control_enabled_cookies']
      },
      {
        name: {
          en: 'User Accounts Cookies'
        },
        description: {
          en: 'This cookie is used to authenticate or remember your account details when you login.'
        },
        cookies: ['PHP_SESSION', '$auth']
      }
    ],
  },
  axios: {
    baseURL: process.env.BACKEND_ADDR ,
  },
  auth: {
    strategies: {
      local: {
        user: {
          property: false,
          autoFetch: false,
        },
        endpoints: {
          login: {
            url: 'wp-json/headless-blog/v1/login',
            method: 'post',
          },
          logout: {
            url: 'wp-json/headless-blog/v1/logout',
            method: 'post'
          },
          user: {
            url: 'wp-json/headless-blog/v1/profile',
            method: 'get',
          }
        },
      }
    }
  },
  dayjs: {
    locales: ['en'],
    defaultLocale: 'en',
    defaultTimeZone: 'Europe/London',
    plugins: [
      'utc', // import 'dayjs/plugin/utc'
      'timezone' // import 'dayjs/plugin/timezone'
    ]
  },
  tailwindcss: {
    viewer: false
  },
  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    analyze: false,
  },
})
